//
//  ScanditCodingQuizTests.swift
//  ScanditCodingQuizTests
//
//  Created by Michal Zabielski on 01.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import XCTest
@testable import ScanditCodingQuiz

class List1 : Listener {
    private let name: String
    private let block: (Void)->Void
    init(name: String, block: @escaping (Void)->Void){
        self.name = name
        self.block = block
    }
    
    func faceRecognizer(_ faceRecognizer: FaceRecognizer, didProcess frame: UIImage, result: Result) {
        print("\(name) HAS LISTENED FROM: \(faceRecognizer), this frame: \(frame), this result: \(result)")
        block()
    }
}
class ScanditCodingQuizTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let mfc = MyFaceRecognizer1Queue()
        print(mfc.recognitionSettings)
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testExample1Queue(){
        executeTest(faceRecognizer: MyFaceRecognizer1Queue())
    }
    
    func testExample2Queues(){
        executeTest(faceRecognizer: MyFaceRecognizer2Queues())
    }
    
    func testExampleFrameSkipping(){
        // fails, because it will probably skip several frames which counts in order to
        // fulfill expectation
        executeTest(faceRecognizer: MyFaceRecognizerFrameSkipping())
    }
    
    func executeTest(faceRecognizer: FaceRecognizer){
        var mfc = faceRecognizer
        let count = counter()
        let exp1 = expectation(description: "OK")
        
        let one = List1(name: "ONE"){}
        let two = List1(name: "TWO"){}
        let three = List1(name: "Three"){
            if count() == 5 {
                exp1.fulfill()
            }
        }
        mfc.add(listener: one)
        mfc.add(listener: two)
        mfc.add(listener: three)
        mfc.recognitionSettings = Settings()
        mfc.recognitionSettings = Settings()
        mfc.process(frame: UIImage(), queue: .main)
        Thread.sleep(forTimeInterval: 0.05)
        mfc.process(frame: UIImage(), queue: .main)
        mfc.recognitionSettings = Settings()
        mfc.recognitionSettings = Settings()
        mfc.remove(listener: one)
        //Thread.sleep(forTimeInterval: 0.25)
        mfc.process(frame: UIImage(), queue: .main)
        //Thread.sleep(forTimeInterval: 0.05)
        mfc.process(frame: UIImage(), queue: .main)
        //Thread.sleep(forTimeInterval: 0.05)
        mfc.process(frame: UIImage(), queue: .main)
        
        waitForExpectations(timeout: 100)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}


func counter()->()->Int{
    var count = 0
    return {
        count = count + 1
        return count
    }
}
