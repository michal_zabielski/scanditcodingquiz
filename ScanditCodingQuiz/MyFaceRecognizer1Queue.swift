//
//  MyFaceRecognizer1.swift
//  ScanditCodingQuiz
//
//  Created by Michal Zabielski on 01.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

// Pure swift. Without Objective-C
// Listeners with strong references.
// No frame skipping
// one queue for execution and settings synchronization
// naive - changes to settings and listeners are queued in the same queue as recognition execution

import Foundation

class MyFaceRecognizer1Queue: FaceRecognizer {
    
    //Every action is synchronized on internal queue.
    private let internalQueue: DispatchQueue = DispatchQueue(label: "MyFaceRecognizer") //serial queue
    
    private let recognitionAlgorithm: RecognitionAlgorithm
    private var _recognitionSettings: Settings
    
    // this indicates whether settings has changed and need to be applied.
    // i cannot compare two Settings instances (it's struct,
    // so I cannot pull the trick with class address.
    // I can only postpone setting application to the suitable moment,
    // and therefore drop all setting apllicances which will be unnecesarry.
    private var settingsHasChanged: Bool = false
    
    private var listeners: [Listener] = []
    
    //I assume that algorithm and settings might be injected to the FaceRecognizer
    init(algorithm: RecognitionAlgorithm, settings: Settings){
        recognitionAlgorithm = algorithm
        _recognitionSettings = settings
        
        // initial application of settings (no problem with synchronization,
        // because it happends during class initialization
        algorithm.apply(settings: settings)
    }
    
    //or as well are initialized during FaceRecognizer class initizaliztion. It was not specified.
    convenience init(){
        self.init(algorithm: RecognitionAlgorithm(), settings: Settings())
    }
    
    func add(listener: Listener){
        internalQueue.async {
            [weak self] in
            guard let strongSelf = self else { return }
            //warning: adds listener and keeps strong reference to it.
            //         Also you can add the same listener more than once.
            //         In such case listener would be executed more than once
            strongSelf.listeners.append(listener)
        }
    }
    
    func remove(listener: Listener){
        internalQueue.async {
            [weak self] in
            guard let strongSelf = self else { return }
            
            //cannot assume almost anything about Listener protocol adopters.
            //Only that they are class objects. And classes have memory adressess
            let listenerAddr = Unmanaged<AnyObject>.passUnretained(listener as AnyObject).toOpaque()
            
            //removes all occureneces of given listener
            strongSelf.listeners = strongSelf.listeners.filter { listener in
                let lAddr = Unmanaged<AnyObject>.passUnretained(listener as AnyObject).toOpaque()
                return lAddr != listenerAddr
            }
        }
    }
    
    /// The setter applies the recognitionSettings to the RecognitionAlgorithm.
    
    // Protocol implies that Settings are never nil.
    // It means that some settings are passed on object creation.
    // The getter in protocol specification makes some troubles.
    // It should also be synchronized in some way.
    var recognitionSettings: Settings {
        get {
            return internalQueue.sync {
                return _recognitionSettings
            }
        }
        
        set {
            // this way the settings would be applied safely (but not least often,
            // as described in assignment)
            internalQueue.async {
                [weak self] in
                guard let strongSelf = self else {return}
                strongSelf._recognitionSettings = newValue
                strongSelf.settingsHasChanged = true
            }
        }
        
    }
    
    
    /// Process a frame and invokes the listeners on the specified queue.
    ///
    /// - parameter frame: The frame to process.
    /// - parameter queue: The queue on which the listeners are invoked.
    func process(frame: UIImage, queue: DispatchQueue){
        internalQueue.async {
            [weak self] in
            guard let strongSelf = self else {return}
            
            //postponed settings application
            if(strongSelf.settingsHasChanged){
                strongSelf.settingsHasChanged = false
                strongSelf.recognitionAlgorithm.apply(settings: strongSelf._recognitionSettings)
            }
            
            let result = strongSelf.recognitionAlgorithm.process(frame: frame)
            let listeners = strongSelf.listeners
            queue.async {
                for l in listeners {
                    l.faceRecognizer(strongSelf, didProcess: frame, result: result)
                }
            }
        }
    }
}
