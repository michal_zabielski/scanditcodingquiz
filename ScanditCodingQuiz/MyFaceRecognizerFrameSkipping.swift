//
//  MyFaceRecognizerFrameSkipping.swift
//  ScanditCodingQuiz
//
//  Created by Michal Zabielski on 02.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

// Pure swift. Without Objective-C
// Listeners with strong references.
// With frame skipping
// one queue for execution and one for settings synchronization

import Foundation

class MyFaceRecognizerFrameSkipping: FaceRecognizer {
    
    // Execution is synchronized on this queue.
    private let executionQueue: DispatchQueue = DispatchQueue(label: "MyFaceRecognizer - execution") //serial queue
    
    // Access to settings is synchronized on this queue.
    // This way access to settings is not enqueued with processing frames,
    // and  new settings can be applied earlier to the RecognitionAlgorithm
    private let synchronizationQueue: DispatchQueue = DispatchQueue(label: "MyFaceRecognizer - synchronization") //serial queue
    
    private let recognitionAlgorithm: RecognitionAlgorithm
    private var _recognitionSettings: Settings
    
    // this indicates whether settings has changed and need to be applied.
    // i cannot compare two Settings instances (it's struct,
    // so I cannot pull the trick with class address.
    // I can only postpone setting application to the suitable moment,
    // and therefore drop all setting apllicances which will be unnecesarry.
    private var settingsHasChanged: Bool = false
    
    private var isCurrentlyProcessing: Bool = false
    
    //request waiting for processing
    private var requestWaitingForProcessing: (image: UIImage, targetQueue: DispatchQueue)?
    
    
    private var listeners: [Listener] = []
    
    //I assume that algorithm and settings might be injected to the FaceRecognizer
    init(algorithm: RecognitionAlgorithm, settings: Settings){
        recognitionAlgorithm = algorithm
        _recognitionSettings = settings
        
        // initial application of settings (no problem with synchronization,
        // because it happends during class initialization
        algorithm.apply(settings: settings)
    }
    
    //or as well are initialized during FaceRecognizer class initizaliztion. It was not specified.
    convenience init(){
        self.init(algorithm: RecognitionAlgorithm(), settings: Settings())
    }
    
    func add(listener: Listener){
        synchronizationQueue.sync {
            //warning: adds listener and keeps strong reference to it.
            //         Also you can add the same listener more than once.
            //         In such case listener would be executed more than once
            listeners.append(listener)
        }
    }
    
    func remove(listener: Listener){
        synchronizationQueue.sync {
            
            //cannot assume almost anything about Listener protocol adopters.
            //Only that they are class objects. And classes have memory adressess
            let listenerAddr = Unmanaged<AnyObject>.passUnretained(listener as AnyObject).toOpaque()
            
            //removes all occureneces of given listener
            listeners = listeners.filter { listener in
                let lAddr = Unmanaged<AnyObject>.passUnretained(listener as AnyObject).toOpaque()
                return lAddr != listenerAddr
            }
        }
    }
    
    /// The setter applies the recognitionSettings to the RecognitionAlgorithm.
    
    // Protocol implies that Settings are never nil.
    // It means that some settings are passed on object creation.
    // The getter in protocol specification makes some troubles.
    // It should also be synchronized in some way.
    var recognitionSettings: Settings {
        get {
            return synchronizationQueue.sync {
                return _recognitionSettings
            }
        }
        
        set {
            // this way the settings would be applied safely (but not least often,
            // as described in assignment)
            synchronizationQueue.sync {
                _recognitionSettings = newValue
                settingsHasChanged = true
            }
        }
        
    }
    
    /// Process a frame and invokes the listeners on the specified queue.
    ///
    /// - parameter frame: The frame to process.
    /// - parameter queue: The queue on which the listeners are invoked.
    func process(frame: UIImage, queue: DispatchQueue){
        let isCurrentlyProcessing: Bool = synchronizationQueue.sync {
            if (self.isCurrentlyProcessing) {
                self.requestWaitingForProcessing = (image: frame, targetQueue: queue)
            }
            return self.isCurrentlyProcessing
        }
        
        if(isCurrentlyProcessing){
            return
        }
        
        executionQueue.async {
            [weak self] in
            guard let strongSelf = self else {return}

            strongSelf.synchronizationQueue.sync {
                strongSelf.isCurrentlyProcessing = true
            }
            
            //getting settings with synchronization mechanism
            let newSettings: Settings? = strongSelf.synchronizationQueue.sync {
                if strongSelf.settingsHasChanged {
                    strongSelf.settingsHasChanged = false
                    return strongSelf._recognitionSettings
                }
                return nil
            }
            
            //postponed settings application in execution queue
            if let settings = newSettings {
                strongSelf.recognitionAlgorithm.apply(settings: settings)
            }
            
            let listeners = strongSelf.synchronizationQueue.sync { strongSelf.listeners }
            
            //execution of frame with which method was executed
            let result = strongSelf.recognitionAlgorithm.process(frame: frame)
            queue.async {
                for l in listeners {
                    l.faceRecognizer(strongSelf, didProcess: frame, result: result)
                }
            }
            
            //check and possible execution of frame which came after method invocation
            if let (frame, queue) = strongSelf.requestWaitingForProcessing {
                strongSelf.requestWaitingForProcessing = nil //reset waiting room
                let result = strongSelf.recognitionAlgorithm.process(frame: frame)
                queue.async {
                    for l in listeners {
                        l.faceRecognizer(strongSelf, didProcess: frame, result: result)
                    }
                }
            }
         
            strongSelf.synchronizationQueue.sync {
                strongSelf.isCurrentlyProcessing = false
            }
        }
    }
}
