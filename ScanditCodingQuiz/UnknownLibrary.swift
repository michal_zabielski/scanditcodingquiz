//
//  UnknownLibrary.swift
//  ScanditCodingQuiz
//
//  Created by Michal Zabielski on 01.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

/// Holds all settings for the RecognitionAlgorithm. Intentionally left empty.
public struct Settings { /* ... */ }
/// Holds the result of the RecognitionAlgorithm. Intentionally left empty.
public struct Result { /* ... */ }
/// The amazing RecognitionAlgorithm
public class RecognitionAlgorithm {
    func apply(settings: Settings) { print("Changed settings!") }
    func process(frame: UIImage) -> Result {
        let result = Result()
        let randomSleep = drand48()
        // beware print occurence is not thread safe :)
        print("Started Processing")
        Thread.sleep(forTimeInterval: randomSleep)
        print("Ended Processing")
        return result
    }
}
