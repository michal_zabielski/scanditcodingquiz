//
//  FaceRecognizer.swift
//  ScanditCodingQuiz
//
//  Created by Michal Zabielski on 01.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

protocol Listener: class {
    func faceRecognizer(_ faceRecognizer: FaceRecognizer,
                        didProcess frame: UIImage,
                        result: Result)
}

/// The protocol that needs to be implemented
protocol FaceRecognizer {
    /// Add a listener
    func add(listener: Listener)
    /// Remove a listener
    func remove(listener: Listener)
    /// The setter applies the recognitionSettings to the RecognitionAlgorithm.
    var recognitionSettings: Settings { get set }
    /// Process a frame and invokes the listeners on the specified queue.
    ///
    /// - parameter frame: The frame to process.
    /// - parameter queue: The queue on which the listeners are invoked.
    func process(frame: UIImage, queue: DispatchQueue)
}
